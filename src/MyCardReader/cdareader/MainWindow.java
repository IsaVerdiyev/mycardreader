package MyCardReader.cdareader;


import MyCardReader.Services.*;
import MyCardReader.cdareader.helper.CDAIntegration;
import MyCardReader.cdareader.helper.MyDomesticData;

import MyCardReader.cdareader.helper.Util;
import com.trueb.cda.lib.CustomerDomesticData;
import sun.applet.Main;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class MainWindow
        extends JFrame
{
    private CDAIntegration cDAIntegration = null;
    private CardTerminal selectedCardTerminal = null;
    private Util utility = new Util();
    private volatile boolean isWorking = false;
    private Thread readerThread = null;
    private boolean cardRemoved = false;
    private JButton btnClose;
    private JButton btnStart;
    private JComboBox jCbCardReader;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JLabel lbCardReaderCaption;
    private JTextField lblAddress;
    private JTextField lblBloodGroup;
    private JLabel lblCaption1;
    private JLabel lblCaption11;
    private JLabel lblCaption12;
    private JLabel lblCaption13;
    private JLabel lblCaption14;
    private JLabel lblCaption15;
    private JLabel lblCaption2;
    private JLabel lblCaption4;
    private JLabel lblCaption5;
    private JLabel lblCaption6;
    private JTextField lblDocumentNumber;
    private JTextField lblEyeColor;
    private JTextField lblFullName;
    private JTextField lblHeight;
    private JTextField lblMaritalStatus;
    private JTextField lblMilitaryStatus;
    private JTextField lblPIN;
    private JTextField lblPlaceOfBirth;
    private JLabel lblStatusBarMessages;
    private JPanel pnlElement1;
    private JPanel pnlElement11;
    private JPanel pnlElement12;
    private JPanel pnlElement13;
    private JPanel pnlElement14;
    private JPanel pnlElement16;
    private JPanel pnlElement2;
    private JPanel pnlElement4;
    private JPanel pnlElement5;
    private JPanel pnlElement6;
    private JPanel pnlOldChipData;
    private JPanel pnlStatusBarMessages;

    public MainWindow()
    {
        initComponents();

        this.lblCaption11.setText(this.utility.getLocalizedText("fullName"));
        this.lblCaption15.setText(this.utility.getLocalizedText("address"));
        this.lblCaption13.setText(this.utility.getLocalizedText("documentNumber"));
        this.lblCaption14.setText(this.utility.getLocalizedText("height"));
        this.lblCaption1.setText(this.utility.getLocalizedText("maritalStatus"));
        this.lblCaption2.setText(this.utility.getLocalizedText("militaryStatus"));
        this.lblCaption4.setText(this.utility.getLocalizedText("bloodGroup"));
        this.lblCaption5.setText(this.utility.getLocalizedText("eyeColor"));
        this.lblCaption6.setText(this.utility.getLocalizedText("placeOfBirth"));

        this.btnClose.setText(this.utility.getLocalizedText("close"));
        try
        {
            this.jCbCardReader.removeAllItems();
            try
            {
                TerminalFactory factory = TerminalFactory.getDefault();
                List<CardTerminal> terminals = factory.terminals().list();
                for (CardTerminal ct : terminals) {
                    this.jCbCardReader.addItem(ct);
                }
                if (this.selectedCardTerminal != null)
                {
                    this.cDAIntegration = new CDAIntegration(this.selectedCardTerminal);
                    this.jCbCardReader.setSelectedItem(this.selectedCardTerminal);
                }
            }
            catch (CardException ex)
            {
                setMessageBarText(true, ex.getMessage());
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 0);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 0);
        }
        setLocationRelativeTo(null);
    }

    public void clearAll()
    {
        this.lblFullName.setText(null);
        this.lblFullName.setToolTipText(null);
        this.lblPIN.setText(null);
        this.lblDocumentNumber.setText(null);
        this.lblHeight.setText(null);
        this.lblMaritalStatus.setText(null);
        this.lblMilitaryStatus.setText(null);
        this.lblBloodGroup.setText(null);
        this.lblEyeColor.setText(null);
        this.lblPlaceOfBirth.setText(null);
        this.lblAddress.setText(null);
        this.lblAddress.setToolTipText(null);
    }

    public void showData(MyDomesticData data)
    {
        clearAll();
        if (data != null)
        {
            this.lblFullName.setText(data.getFullName() != null ? data.getFullName().trim() : "");
            this.lblPIN.setText(data.getFin().trim());
            this.lblDocumentNumber.setText(data.getDocumentNumber().trim());
            this.lblHeight.setText(data.getHeight().trim() + "");
            this.lblMaritalStatus.setText(data.getMaritalStatus().trim());
            this.lblMilitaryStatus.setText(data.getMilitaryStatus().trim());
            this.lblBloodGroup.setText(data.getBloodGroup().trim());
            this.lblEyeColor.setText(data.getEyeColor().trim());
            this.lblPlaceOfBirth.setText(data.getPlaceOfBirth().trim());
            this.lblAddress.setText(data.getAddress() != null ? this.utility.cutAddress(data.getAddress()) : "");
        }
    }

    public void setMessageBarText(boolean isWarning, String text)
    {
        Font font = this.lblStatusBarMessages.getFont();
        font = font.deriveFont(isWarning ? 1 : 0);
        this.lblStatusBarMessages.setForeground(isWarning ? Color.RED : Color.BLACK);
        this.lblStatusBarMessages.setFont(font);
        this.lblStatusBarMessages.setText(text);
    }

    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                MainWindow mw = new MainWindow();
                mw.setTitle("Card Reader");
                mw.setVisible(true);
            }
        });
    }

    private void initComponents()
    {
        this.jPanel1 = new JPanel();
        this.pnlOldChipData = new JPanel();
        this.jPanel4 = new JPanel();
        this.pnlElement11 = new JPanel();
        this.lblCaption11 = new JLabel();
        this.lblFullName = new JTextField();
        this.pnlElement12 = new JPanel();
        this.lblCaption12 = new JLabel();
        this.lblPIN = new JTextField();
        this.pnlElement16 = new JPanel();
        this.lblCaption15 = new JLabel();
        this.lblAddress = new JTextField();
        this.pnlElement13 = new JPanel();
        this.lblCaption13 = new JLabel();
        this.lblDocumentNumber = new JTextField();
        this.pnlElement14 = new JPanel();
        this.lblCaption14 = new JLabel();
        this.lblHeight = new JTextField();
        this.pnlElement1 = new JPanel();
        this.lblCaption1 = new JLabel();
        this.lblMaritalStatus = new JTextField();
        this.pnlElement2 = new JPanel();
        this.lblCaption2 = new JLabel();
        this.lblMilitaryStatus = new JTextField();
        this.pnlElement4 = new JPanel();
        this.lblCaption4 = new JLabel();
        this.lblBloodGroup = new JTextField();
        this.pnlElement5 = new JPanel();
        this.lblCaption5 = new JLabel();
        this.lblEyeColor = new JTextField();
        this.pnlElement6 = new JPanel();
        this.lblCaption6 = new JLabel();
        this.lblPlaceOfBirth = new JTextField();
        this.jPanel2 = new JPanel();
        this.lbCardReaderCaption = new JLabel();
        this.jCbCardReader = new JComboBox();
        this.btnStart = new JButton();
        this.jPanel3 = new JPanel();
        this.jPanel5 = new JPanel();
        this.pnlStatusBarMessages = new JPanel();
        this.lblStatusBarMessages = new JLabel();
        this.jPanel6 = new JPanel();
        this.btnClose = new JButton();

        setDefaultCloseOperation(3);
        setTitle("MainWindowTitle");
        setPreferredSize(new Dimension(800, 450));
        addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent evt)
            {
                MainWindow.this.formWindowClosing(evt);
            }
        });
        this.jPanel1.setLayout(new BorderLayout());

        this.pnlOldChipData.setBackground(new Color(255, 255, 255));
        this.pnlOldChipData.setBorder(BorderFactory.createTitledBorder(null, "ChipData", 0, 0, new Font("Tahoma", 1, 11)));
        this.pnlOldChipData.setLayout(new BorderLayout());

        this.jPanel4.setBackground(new Color(255, 255, 255));
        this.jPanel4.setLayout(new GridLayout(12, 0, 0, 2));

        this.pnlElement11.setBackground(new Color(255, 255, 255));
        this.pnlElement11.setPreferredSize(new Dimension(330, 22));
        this.pnlElement11.setLayout(new BorderLayout());

        this.lblCaption11.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption11.setText("FullName");
        this.lblCaption11.setAlignmentX(0.5F);
        this.lblCaption11.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption11.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption11.setMinimumSize(new Dimension(10, 22));
        this.lblCaption11.setPreferredSize(new Dimension(200, 22));
        this.pnlElement11.add(this.lblCaption11, "West");

        this.lblFullName.setEditable(false);
        this.lblFullName.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement11.add(this.lblFullName, "Center");

        this.jPanel4.add(this.pnlElement11);

        this.pnlElement12.setBackground(new Color(255, 255, 255));
        this.pnlElement12.setPreferredSize(new Dimension(330, 22));
        this.pnlElement12.setLayout(new BorderLayout());

        this.lblCaption12.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption12.setText("PIN");
        this.lblCaption12.setAlignmentX(0.5F);
        this.lblCaption12.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption12.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption12.setMinimumSize(new Dimension(10, 22));
        this.lblCaption12.setPreferredSize(new Dimension(200, 22));
        this.pnlElement12.add(this.lblCaption12, "West");

        this.lblPIN.setEditable(false);
        this.lblPIN.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement12.add(this.lblPIN, "Center");

        this.jPanel4.add(this.pnlElement12);

        this.pnlElement16.setBackground(new Color(255, 255, 255));
        this.pnlElement16.setPreferredSize(new Dimension(330, 22));
        this.pnlElement16.setLayout(new BorderLayout());

        this.lblCaption15.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption15.setText("Address");
        this.lblCaption15.setAlignmentX(0.5F);
        this.lblCaption15.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption15.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption15.setMinimumSize(new Dimension(10, 22));
        this.lblCaption15.setPreferredSize(new Dimension(200, 22));
        this.pnlElement16.add(this.lblCaption15, "West");

        this.lblAddress.setEditable(false);
        this.lblAddress.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement16.add(this.lblAddress, "Center");

        this.jPanel4.add(this.pnlElement16);

        this.pnlElement13.setBackground(new Color(255, 255, 255));
        this.pnlElement13.setPreferredSize(new Dimension(330, 22));
        this.pnlElement13.setLayout(new BorderLayout());

        this.lblCaption13.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption13.setText("DocumentNumber");
        this.lblCaption13.setAlignmentX(0.5F);
        this.lblCaption13.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption13.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption13.setMinimumSize(new Dimension(10, 22));
        this.lblCaption13.setPreferredSize(new Dimension(200, 22));
        this.pnlElement13.add(this.lblCaption13, "West");

        this.lblDocumentNumber.setEditable(false);
        this.lblDocumentNumber.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement13.add(this.lblDocumentNumber, "Center");

        this.jPanel4.add(this.pnlElement13);

        this.pnlElement14.setBackground(new Color(255, 255, 255));
        this.pnlElement14.setPreferredSize(new Dimension(330, 22));
        this.pnlElement14.setLayout(new BorderLayout());

        this.lblCaption14.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption14.setText("Height");
        this.lblCaption14.setAlignmentX(0.5F);
        this.lblCaption14.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption14.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption14.setMinimumSize(new Dimension(10, 22));
        this.lblCaption14.setPreferredSize(new Dimension(200, 22));
        this.pnlElement14.add(this.lblCaption14, "West");

        this.lblHeight.setEditable(false);
        this.lblHeight.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement14.add(this.lblHeight, "Center");

        this.jPanel4.add(this.pnlElement14);

        this.pnlElement1.setBackground(new Color(255, 255, 255));
        this.pnlElement1.setPreferredSize(new Dimension(330, 22));
        this.pnlElement1.setLayout(new BorderLayout());

        this.lblCaption1.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption1.setText("MaritalStatus");
        this.lblCaption1.setAlignmentX(0.5F);
        this.lblCaption1.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption1.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption1.setMinimumSize(new Dimension(10, 22));
        this.lblCaption1.setPreferredSize(new Dimension(200, 22));
        this.pnlElement1.add(this.lblCaption1, "West");

        this.lblMaritalStatus.setEditable(false);
        this.lblMaritalStatus.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement1.add(this.lblMaritalStatus, "Center");

        this.jPanel4.add(this.pnlElement1);

        this.pnlElement2.setBackground(new Color(255, 255, 255));
        this.pnlElement2.setPreferredSize(new Dimension(330, 22));
        this.pnlElement2.setLayout(new BorderLayout());

        this.lblCaption2.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption2.setText("MilitaryStatus");
        this.lblCaption2.setAlignmentX(0.5F);
        this.lblCaption2.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption2.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption2.setMinimumSize(new Dimension(10, 22));
        this.lblCaption2.setPreferredSize(new Dimension(200, 22));
        this.pnlElement2.add(this.lblCaption2, "West");

        this.lblMilitaryStatus.setEditable(false);
        this.lblMilitaryStatus.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement2.add(this.lblMilitaryStatus, "Center");

        this.jPanel4.add(this.pnlElement2);

        this.pnlElement4.setBackground(new Color(255, 255, 255));
        this.pnlElement4.setPreferredSize(new Dimension(330, 22));
        this.pnlElement4.setLayout(new BorderLayout());

        this.lblCaption4.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption4.setText("BloodGroup");
        this.lblCaption4.setAlignmentX(0.5F);
        this.lblCaption4.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption4.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption4.setMinimumSize(new Dimension(10, 22));
        this.lblCaption4.setPreferredSize(new Dimension(200, 22));
        this.pnlElement4.add(this.lblCaption4, "West");

        this.lblBloodGroup.setEditable(false);
        this.lblBloodGroup.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement4.add(this.lblBloodGroup, "Center");

        this.jPanel4.add(this.pnlElement4);

        this.pnlElement5.setBackground(new Color(255, 255, 255));
        this.pnlElement5.setPreferredSize(new Dimension(330, 22));
        this.pnlElement5.setLayout(new BorderLayout());

        this.lblCaption5.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption5.setText("EyeColor");
        this.lblCaption5.setAlignmentX(0.5F);
        this.lblCaption5.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption5.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption5.setMinimumSize(new Dimension(10, 22));
        this.lblCaption5.setPreferredSize(new Dimension(200, 22));
        this.pnlElement5.add(this.lblCaption5, "West");

        this.lblEyeColor.setEditable(false);
        this.lblEyeColor.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement5.add(this.lblEyeColor, "Center");

        this.jPanel4.add(this.pnlElement5);

        this.pnlElement6.setBackground(new Color(255, 255, 255));
        this.pnlElement6.setPreferredSize(new Dimension(330, 22));
        this.pnlElement6.setLayout(new BorderLayout());

        this.lblCaption6.setFont(new Font("Tahoma", 1, 11));
        this.lblCaption6.setText("PlaceOfBirth");
        this.lblCaption6.setAlignmentX(0.5F);
        this.lblCaption6.setBorder(BorderFactory.createEmptyBorder(15, 6, 12, 0));
        this.lblCaption6.setMaximumSize(new Dimension(1000, 22));
        this.lblCaption6.setMinimumSize(new Dimension(10, 22));
        this.lblCaption6.setPreferredSize(new Dimension(200, 22));
        this.pnlElement6.add(this.lblCaption6, "West");

        this.lblPlaceOfBirth.setEditable(false);
        this.lblPlaceOfBirth.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent evt)
            {
                MainWindow.this.tfMouseClicked(evt);
            }
        });
        this.pnlElement6.add(this.lblPlaceOfBirth, "Center");

        this.jPanel4.add(this.pnlElement6);

        this.pnlOldChipData.add(this.jPanel4, "Center");

        this.jPanel1.add(this.pnlOldChipData, "Center");

        getContentPane().add(this.jPanel1, "Center");

        this.jPanel2.setBackground(new Color(255, 255, 255));
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(1, 5, 1, 1));
        this.jPanel2.setPreferredSize(new Dimension(468, 50));
        this.jPanel2.setLayout(new FlowLayout(0, 5, 7));

        this.lbCardReaderCaption.setFont(new Font("Tahoma", 1, 11));
        this.lbCardReaderCaption.setText("CardReader");
        this.jPanel2.add(this.lbCardReaderCaption);

        this.jCbCardReader.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        this.jCbCardReader.setPreferredSize(new Dimension(400, 20));
        this.jPanel2.add(this.jCbCardReader);

        this.btnStart.setText("Start");
        this.btnStart.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                MainWindow.this.btnStartActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.btnStart);

        getContentPane().add(this.jPanel2, "First");

        this.jPanel3.setPreferredSize(new Dimension(400, 36));
        this.jPanel3.setLayout(new BorderLayout(0, 2));

        this.jPanel5.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this.jPanel5.setLayout(new BorderLayout());

        this.pnlStatusBarMessages.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createBevelBorder(1)));
        this.pnlStatusBarMessages.setFont(new Font("Tahoma", 1, 11));
        this.pnlStatusBarMessages.setPreferredSize(new Dimension(150, 19));
        this.pnlStatusBarMessages.setLayout(new CardLayout());

        this.lblStatusBarMessages.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        this.pnlStatusBarMessages.add(this.lblStatusBarMessages, "card2");

        this.jPanel5.add(this.pnlStatusBarMessages, "Center");

        this.jPanel3.add(this.jPanel5, "Center");

        this.jPanel6.setPreferredSize(new Dimension(120, 100));
        this.jPanel6.setLayout(new FlowLayout(2));

        this.btnClose.setIcon(new ImageIcon(getClass().getResource("/MyCardReader/cdareader/resources/close.png")));
        this.btnClose.setText("Close");
        this.btnClose.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                MainWindow.this.btnCloseActionPerformed(evt);
            }
        });
        this.jPanel6.add(this.btnClose);

        this.jPanel3.add(this.jPanel6, "East");

        getContentPane().add(this.jPanel3, "Last");

        pack();
    }

    private void btnCloseActionPerformed(ActionEvent evt)
    {
        formWindowClosing(null);
    }

    private void btnStartActionPerformed(ActionEvent evt)
    {
        try
        {
            if (this.isWorking) {
                throw new Exception("ALREADY_STARTED");
            }
            if (this.jCbCardReader.getSelectedItem() == null) {
                throw new Exception("ERR_CARD_READER_NOT_FOUND");
            }
            if (this.selectedCardTerminal == null)
            {
                this.selectedCardTerminal = ((CardTerminal)this.jCbCardReader.getSelectedItem());
                this.cDAIntegration = new CDAIntegration(this.selectedCardTerminal);
            }
            else if (!this.selectedCardTerminal.equals((CardTerminal)this.jCbCardReader.getSelectedItem()))
            {
                this.selectedCardTerminal = ((CardTerminal)this.jCbCardReader.getSelectedItem());
                this.cDAIntegration = new CDAIntegration(this.selectedCardTerminal);
            }
            this.isWorking = true;
            this.cardRemoved = true;
            this.btnStart.setEnabled(false);
            this.readerThread = new Thread(new Runnable()
            {
                public void run()
                {
                    while (MainWindow.this.isWorking) {
                        try
                        {
                            if (MainWindow.this.cardRemoved) {
                                MainWindow.this.selectedCardTerminal.waitForCardPresent(2000L);
                            } else {
                                MainWindow.this.selectedCardTerminal.waitForCardAbsent(2000L);
                            }
                            if ((MainWindow.this.selectedCardTerminal.isCardPresent()) && (MainWindow.this.cardRemoved))
                            {
                                MainWindow.this.cardRemoved = false;
                                MainWindow.this.clearAll();
                                CustomerDomesticData readData = MainWindow.this.cDAIntegration.readData();
                                MyDomesticData domesticData = new MyDomesticData((readData));
                                MainWindow.this.showData(domesticData);
                                //-------------My code -------------------------
                                try {
                                    IPropertiesGetter propertiesGetter = new PropertiesGetter("MyCardReader/Services/resources/config.properties");
                                    Connection connection = new DbConnectionService(propertiesGetter).GetConnection();
                                    IDataSaver dataSaver = new DbDataSaver(propertiesGetter, connection);
                                    dataSaver.WriteData(domesticData);
                                    connection.close();
                                }catch (Exception ex){
                                    Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                                    JOptionPane.showMessageDialog(null, MainWindow.this.utility.getLocalizedText("Database Exception occured"));
                                }
                                //----------------------------------------------
                                MainWindow.this.setMessageBarText(false, MainWindow.this.utility.getLocalizedText("IDCARD_CHIP_SUCCESFULLY_READ"));
                                JOptionPane.showMessageDialog(null, MainWindow.this.utility.getLocalizedText("IDCARD_CHIP_SUCCESFULLY_READ"), "INFORMATION", 1);
                            }
                            else if (!MainWindow.this.selectedCardTerminal.isCardPresent())
                            {
                                MainWindow.this.cardRemoved = true;
                            }
                        }
                        catch (CardException ex)
                        {
                            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        catch (Exception ex)
                        {
                            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            this.readerThread.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void formWindowClosing(WindowEvent evt)
    {
        this.isWorking = false;
        if (this.readerThread != null) {
            while (this.readerThread.isAlive())
            {
                this.isWorking = false;
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        System.exit(0);
    }

    private void tfMouseClicked(MouseEvent evt)
    {
        JTextField tf = (JTextField)evt.getSource();
        tf.selectAll();
        StringSelection data = new StringSelection(tf.getText());
        Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
        cb.setContents(data, data);
    }
}
