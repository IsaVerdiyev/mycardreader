package MyCardReader.cdareader.helper;

import com.trueb.cda.lib.CDALibrary;
import com.trueb.cda.lib.CertificateData;
import java.io.PrintStream;
import java.util.List;
import javax.smartcardio.CardTerminal;

public class CDAIntegration
        extends CDALibrary
{
    public CDAIntegration(CardTerminal aTerminal)
    {
        super(aTerminal);
    }

    public byte[] callbackSignChallenge(String name, byte[] aChallenge)
    {
        return null;
    }

    public List<CertificateData> callbackCertificate(String certName)
    {
        return null;
    }

    public void callbackErrorMessage(String string)
    {
        System.err.println("callbackErrorMessage: " + string);
    }
}
