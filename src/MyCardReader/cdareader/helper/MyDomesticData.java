package MyCardReader.cdareader.helper;

import com.trueb.cda.lib.CustomerDomesticData;
import java.io.UnsupportedEncodingException;

public class MyDomesticData
{
    private static final String CHARACTER_SET_NAME = "UTF-8";
    private CustomerDomesticData cdd;

    public MyDomesticData(CustomerDomesticData aCdaData)
    {
        this.cdd = aCdaData;
    }

    public void setCdaData(CustomerDomesticData aCdaData)
    {
        this.cdd = aCdaData;
    }

    public CustomerDomesticData getCdaData()
    {
        return this.cdd;
    }

    public String getBloodGroup()
    {
        int offSet = 0;
        int dataSize = 12;
        return extractData(offSet, dataSize);

    }

    public String getEyeColor()
    {
        int offSet = 12;
        int dataSize = 20;
        return extractData(offSet, dataSize);

    }

    public String getPlaceOfBirth()
    {
        int offSet = 32;
        int dataSize = 128;
        return extractData(offSet, dataSize);
        /*return "Baku";*/
    }

    public String getFullName()
    {
        int offSet = 160;
        int dataSize = 128;
        return extractData(offSet, dataSize);
        /*return "Üzeyir Flankəsov";*/
    }

    public String getFin()
    {
        int offSet = 288;
        int dataSize = 10;
        return extractData(offSet, dataSize);
        /*return "dsdfsdf";*/
    }

    public String getDocumentNumber()
    {
        int offSet = 298;
        int dataSize = 10;
        return extractData(offSet, dataSize);
        /*return "AZE2397219";*/
    }

    public String getAddress()
    {
        int offSet = 436;
        int dataSize = 512;
        return extractData(offSet, dataSize);
        /*return "Naxçivanski 23";*/
    }

    public String getMaritalStatus()
    {
        int offSet = 948;
        int dataSize = 20;
        return extractData(offSet, dataSize);
        /*return "single";*/
    }

    public String getMilitaryStatus()
    {
        int offSet = 968;
        int dataSize = 20;
        return extractData(offSet, dataSize);
       /* return "was";*/
    }

    public String getHeight()
    {
        int offSet = 988;
        int dataSize = 3;
        return extractData(offSet, dataSize);
        /*return "428";*/
    }

    protected String extractData(int offSet, int dataSize)
    {
        String curValue = null;
        try
        {
            curValue = new String(this.cdd.getData(), offSet, dataSize, "UTF-8");
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
        return curValue;
    }
}
