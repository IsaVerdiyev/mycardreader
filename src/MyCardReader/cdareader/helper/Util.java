package MyCardReader.cdareader.helper;

import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
    private static final ResourceBundle CONFIG_RB = ResourceBundle.getBundle("MyCardReader.cdareader.resources.localization");
    private Locale locale = null;
    private InputStream input = null;
    private Properties prop = new Properties();

    public String getLocalizedText(String key)
    {
        try
        {
            return CONFIG_RB.getString(key);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "property not found";
    }

    public String cutAddress(String address)
    {
        Matcher m = Pattern.compile("1,[^,]+,([^;]*);").matcher(address);
        if (m.find()) {
            address = m.group(1);
        }
        return address.trim();
    }
}
