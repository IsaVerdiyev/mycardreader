package MyCardReader.Services;

import javax.naming.NamingException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface IDbConnectionService {
    public Connection GetConnection() throws SQLException, ClassNotFoundException, NamingException, IOException;
}
