package MyCardReader.Services;

public interface IPropertiesGetter {
    public String GetPropValue(String propName);
}
