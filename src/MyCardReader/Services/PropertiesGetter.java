package MyCardReader.Services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesGetter implements  IPropertiesGetter {

    private String propFileName;
    private Properties properties;

    public PropertiesGetter(String fileName) throws IOException {
        properties = new Properties();
        this.propFileName = fileName;
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        if(inputStream !=  null) {
            properties.load(inputStream);
        } else{
            throw new FileNotFoundException("Property file '" + propFileName + "' not found in the classpath");
        }
    }

    @Override
    public String GetPropValue(String propName) {
        return properties.getProperty(propName);
    }
}
