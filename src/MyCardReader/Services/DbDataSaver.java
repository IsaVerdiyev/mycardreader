package MyCardReader.Services;


import MyCardReader.cdareader.helper.MyDomesticData;

import javax.naming.NamingException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class DbDataSaver implements IDataSaver {


    private IPropertiesGetter propertiesGetter;
    private Connection connection;

    public DbDataSaver(IPropertiesGetter propertiesGetter, Connection connection) {

        this.propertiesGetter = propertiesGetter;
        this.connection = connection;
    }

    @Override
    public MyDomesticData WriteData(MyDomesticData data) throws SQLException, NamingException, ClassNotFoundException, IOException {
        PreparedStatement preparedStatement;

        preparedStatement = connection.prepareStatement("insert into " + propertiesGetter.GetPropValue("tableName") + "(" +
                "FullName, Fin, DocumentNumber, Height, MaritalStatus, MilitaryStatus, BloodGroup, EyeColor, PlaceOfBirth, Address, DateOfInput) " +
                "Values (" +
                "'" + data.getFullName() + "', " +
                "'" + data.getFin() + "', " +
                "'" + data.getDocumentNumber() + "', " +
                "'" + data.getHeight() + "', " +
                "'" + data.getMaritalStatus() + "', " +
                "'" + data.getMilitaryStatus() + "', " +
                "'" + data.getBloodGroup() + "', " +
                "'" + data.getEyeColor() + "', " +
                "'" + data.getPlaceOfBirth() + "', " +
                "'" + data.getAddress() + "', " +
                "CURRENT_TIMESTAMP)");
        preparedStatement.executeUpdate();

        return data;
    }

   /* @Override
    public Connection CreateDatabase(Connection connection, String databaseName) throws SQLException {
        PreparedStatement preparedStatement;
        preparedStatement = connection.prepareStatement("CREATE DATABASE " + databaseName + ";");

        preparedStatement.executeUpdate();
        return connection;
    }

    @Override
    public Connection CreateTable(Connection connection, String tableName) throws SQLException, NamingException, ClassNotFoundException {
        PreparedStatement preparedStatement;

        preparedStatement = connection.prepareStatement("CREATE TABLE " + tableName + " (" +
                "Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "FullName nvarchar (100) NOT NULL, " +
                "Fin nvarchar(15) NOT NULL, " +
                "DocumentNumber nvarchar(15) NOT NULL, " +
                "Height nvarchar(15) NOT NULL, " +
                "MaritalStatus nvarchar(50) NOT NULL, " +
                "MilitaryStatus nvarchar(50) NOT NULL, " +
                "BloodGroup nvarchar(40) NOT NULL, " +
                "EyeColor nvarchar(40) NOT NULL, " +
                "PlaceOfBirth nvarchar(200) NOT NULL, " +
                "Address nvarchar(200) NOT NULL)");

        preparedStatement.executeUpdate();
        return connection;
    }
*/
    /*@Override
    public Connection CreateDataBaseAndTableIfNotExists(Connection connection, String dbName, String tableName) throws SQLException, NamingException, ClassNotFoundException {
        ResultSet catalogs = connection.getMetaData().getCatalogs();
        boolean isOk = false;
        *//*while(catalogs.next()){
            String databaseName = catalogs.getString(1);
            if(databaseName.equals(dbName)){
                isOk = true;
            }
        }
        if(!isOk){
            CreateDatabase(connection, dbName);
        }*//*


        ResultSet tables = connection.getMetaData().getTables(null, null, tableName, null);
        while(tables.next()) {
            return connection;
        }
        CreateTable(connection, tableName);
        return connection;
    }*/
}
