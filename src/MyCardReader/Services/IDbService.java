package MyCardReader.Services;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;

public interface IDbService {
    public Connection CreateDatabase(Connection connection, String databaseName) throws SQLException;
    public Connection CreateTable(Connection connection, String tableName) throws SQLException, NamingException, ClassNotFoundException;
    //public  Connection CreateDataBaseAndTableIfNotExists(Connection connection, String databaseName, String tableName) throws SQLException, NamingException, ClassNotFoundException;
}
