package MyCardReader.Services;


import MyCardReader.cdareader.helper.MyDomesticData;

import javax.naming.NamingException;
import java.io.IOException;
import java.sql.SQLException;

public interface IDataSaver {
    public MyDomesticData WriteData(MyDomesticData data) throws SQLException, NamingException, ClassNotFoundException, IOException;

}
