package MyCardReader.Services;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnectionService implements  IDbConnectionService {


    private IPropertiesGetter propertiesGetter;

    public DbConnectionService(IPropertiesGetter propertiesGetter) {

        this.propertiesGetter = propertiesGetter;
    }

    public Connection GetConnection() throws SQLException, ClassNotFoundException, NamingException, IOException {
        Connection c;

        c = DriverManager.getConnection(propertiesGetter.GetPropValue("conString"), propertiesGetter.GetPropValue("user"), propertiesGetter.GetPropValue("password"));

        return c;
    }
    /*public Connection GetConnection() throws SQLException, ClassNotFoundException, NamingException, IOException{
        String url = propertiesGetter.GetPropValue("conString");
        String driver = propertiesGetter.GetPropValue("driver");
        String userName =propertiesGetter.GetPropValue("user");
        String pass= propertiesGetter.GetPropValue("password");
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url,userName,pass);
        } catch (Exception e) {
            System.out.println(e);
        }

        return con;
    }*/

    public Connection GetConnection1() throws SQLException, ClassNotFoundException, NamingException, IOException{
        String url = "jdbc:oracle:thin:@172.0.0.129:1521:t24dbwh";
        String driver = "oracle.jdbc.OracleDriver";
        String userName ="IDCARDREADER";
        String pass="IDCRDPASS";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url,userName,pass);
        } catch (Exception e) {
            System.out.println(e);
        }

        return con;
    }
}
